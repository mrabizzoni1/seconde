---
author: Maud RABIZZONI
title: Python
---



## 1. Fonctionnement du cours 

Trois grands types d'exercices sont proposés.

!!! exo "Papier/Crayon"

    Prenez une feuille et un crayon. 

!!! exo "Prédire/comprendre"

    Vous disposez d'un programme dans un éditeur. Vous devez comprendre le programme et prédire ce qui va se passer. Vous pouvez tester en appuyant sur la flèche pointant à droite.

    {{IDEv('exemples/exemple')}}

???+ question "Programmer"

    Vous devez compléter ou écrire un programme dans un éditeur. 

    Vous pouvez tester en appuyant sur la flèche pointant à droite ▶️. 


    {{IDEv('exemples/exemple2')}}

## 2. Interpréteur/Editeur

L’interpréteur (terminal ou shell), on le reconnait facilement car contient le triple chevron >>> qui est l’invite de Python et qui signifie que Python attend une commande. On peut taper des lignes de codes qui sont exécutées instantanément lorsqu’on valide par la touche Entrée.

!!! example "EXEMPLE"
    Taper 2+3 puis valider avec la touche ++enter++
    {{terminal()}}

    Dans l’éditeur, on peut y écrire des scripts, c’est-à-dire des programmes petits ou grands. Pour exécuter le script, il faut cliquer sur la flèche

    {{IDEv('exemples/exemple3')}}

## 3. Premiers pas

???+ question "EXERCICES"
    === "Exercice_1"
        Dans la console, taper `#!python print("Hello world !")` et appuyez sur ++enter++

    === "Exercice_2"
        Dans la console, taper `#!python "Hello world !"` et appuyez sur ++enter++
 
    === "Exercice_4"
        Dans la console, taper 32+5/2+1.5 et appuyez sur ++enter++

    {{terminal()}}

???+ question "EXERCICES"
    === "Exercice_5"
        Dans l'éditeur, taper `#!python print("Hello world !")` puis exécuter en cliquant sur ▶️.
    
    === "Exercice_6"
        Dans l'éditeur, taper `#!python 32+5/2+1.5` puis exécuter en cliquant sur ▶️.
    
    === "Exercice_7"
        Dans l'éditeur, taper `#!python print(32+5/2+1.5)` puis exécuter en cliquant sur ▶️.

    {{IDEv()}}


## 4. Les variables

### 4.1. Les noms


Une variable, dans le domaine de la programmation informatique, est un conteneur qui sert à stocker une valeur.

Les variables possèdent deux caractéristiques fondamentales :  
- Les variables ont une durée de vie limitée (une variable ne vit généralement que le temps de l’exécution d’un script ou de la fonction qui l’a définie);  
- La valeur d’une variable peut varier : les variables peuvent peuvent stocker différentes valeurs au cours de leur vie (la nouvelle valeur remplaçant l’ancienne).  

Le nom d'une variable s'écrit avec des lettres (non accentuées de préférence), des chiffres ou bien un underscore " _ ".   
Le nom d'une variable ne doit pas commencer par un chiffre.


!!! warning "Attention"
    Il existe des mots réservés. En voici quelques-uns :
    `python and, as, assert, break, class, continue, def, del, elif, else, except, False, finally, for, from, global, if, import, in, is, lambda, None, nonlocal, not, or, pass, raise, return, True, try, while, with, yield`

Notez que les noms de variables en Python sont sensibles à la casse, ce qui signifie qu’on va faire une différence entre l’emploi de majuscules et de minuscules : un même nom écrit en majuscules ou en minuscules créera deux variables totalement différentes.

???+ question "VRAI/FAUX"

    Cocher les identifiants valides.

    === "Question"
        - [ ] `toto`
        - [ ] `t0t0`
        - [ ] `3eme`
        - [ ] `Sa_3m7`
        - [ ] `else`
        - [ ] `Test()`
        - [ ] `Mon_Mot`
        - [ ] `Mon mot`
        - [ ] `_avant`
        - [ ] `login@mail`
        - [ ] `après`
        - [ ] `login.mail`
        - [ ] `login_mail`
        - [ ] `_`

    === "Solution"
        - [x] `toto`
        - [x] `t0t0`
        - [ ] `3eme` ; commence par un chiffre.
        - [x] `Sa_3m7`
        - [ ] `else` ; mot réservé.
        - [ ] `Test()` ; interdit d'utiliser les parenthèses.
        - [x] `Mon_Mot`
        - [ ] `Mon mot` ; pas d'espace.
        - [x] `_avant`
        - [ ] `login@mail` ; pas de @.
        - [ ] `après` ; non conforme mais valide
        - [ ] `login.mail` ; pas de point.
        - [x] `login_mail`
        - [x] `_`



### 4.2. L'affectation

Pour affecter ou assigner une valeur à une variable, nous allons utiliser un opérateur qu’on appelle opérateur d’affectation ou d’assignation et qui est représenté par le signe `=`. 

!!! warning "Attention"
    Lee signe `=` ne signifie pas en informatique l’égalité d’un point de vue mathématique : c’est un opérateur d’affectation.

!!! info "INFO"
    En python, une variable contient une référence (une sorte d'adresse) vers un objet contenant une valeur.
    La fonction `id(nom_variable)` renvoie la référence de la variable `nom_variable`.


On différenciera :  
- L'initialisation : c'est l'association initiale d'un contenu avec une valeur (1ère valeur) ;  
- L'affectation : c'est l'association d'un contenu avec une variable déjà initialisée ;  
- L'incrémentation : c'est l'augmentation régulière de la valeur associée à une variable .


!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire0')}}

!!! info "REMARQUE"
    En Python, de nombreux objets sont IMMUABLES (on ne peut pas les modifier). Cela se traduit par un changement de référence.


## 5. Les types

### 5.1. Le type int (integer : nbs entiers)

En Python, les variables ont toutes un type qui correspend à la nature de la valeur stockée dans la variable : nombre entier, nombre réel, chaîne de caractères, booléen (Vrai ou Faux), …

Contrairement à beaucoup d'autres langages, en Python, il n'est pas nécessaire de déclarer le type des variables lors de leur création : Python attribue automatiquement un type à une variable lors de sa création, on parle de typage dynamique.



???+ question "EXERCICE"
    Dans l'éditeur, créer une variable age en lui donnant la valeur 15.  
    Afficher dans la console la valeur de la variable en utilisant la fonction `print(nom_de_la_variable)`.  
    Taper  `print(type(age))`  puis executer.

    {{IDEv()}}



!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire1')}}
	

Sur ce type de variables, on peut faire différentes opérations :  
- L’addition avec le symbole « + ».  
- La soustraction avec le symbole « - ».  
- La multiplication avec le symbole « * ».  
- La division avec le symbole « / ».  
- La division entière avec le symbole « // » (le résultat est la partie entière de la division).  
- Le modulo avec le symbole « % » (le résultat est le reste de la division).  
- La puissance avec le symbole « ** ».  

???+ question "EXERCICE"
    Ecrire les lignes de codes pour répondre au cahier des charges suivant :  
    Créer une variable `a = 6*3+2`.  
    Créer une variable `b = 20`.  
    Créer une variable `c = a+2*b`.  
    Afficher dans la même ligne `a`, `b` et `c`.  
    Créer une variable `angle = 450`.  
    Créer une variable `tour` et calculer le nombre de tour entier qu’il y a dans angle.  
    Créer une variable `reste` et calculer l’angle restant (on utilisera `%`).  
    Afficher sur la même ligne `angle`, `tour` et `reste`.  
    Créer une variable `puissance` qui vaut 2 exposant 20 et afficher le résultat et son type.  
    Créer une variable `racine2` qui sera la racine carrée de 2.

    {{IDEv()}}

    ??? success "Solution"
        ```python
        a=6*3+2
        b=20
        c=a+2*b
        print(a,b,c)

        angle=450
        tour=angle//180
        reste=angle%180
        print(angle,tour,reste)
        puissance=2**20
        print(puissance,type(puissance))
        racine2=2**0.5 # ou racine2=sqrt(2) 
        ```



### 5.2. Le type float (floating : nombres en virgule flottante)

Pour déclarer un float, il suffit de mettre le séparateur décimal qui est un point sous python.

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDEv('predires/predire2')}}


On peut aussi écrire des réels au format scientifique avec la lettre e.

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDEv('predires/predire3')}}


On peut aussi utiliser des fonctions mathématiques existantes. Pour cela, il faut importer le module math.  
Pour connaitre toutes les fonctions d’un module, il suffit d’utiliser la fonction `dir(nom du module)`.  
Pour utiliser une fonction d’un module, il suffit d'importer le module (ou librairie ou bibliothèque).

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire4')}}


???+ question "EXERCICE"
    Créer une variable Pi et lui affecter la valeur $\Pi$.  
    Créer la variable cosPi est lui affecter la valeut de cosinus $\Pi$ .

    {{IDEv('scripts/exo1')}}





### 5.3. Le type str (string : chaîne de caractères)

Pour créer une variable str, il faut l’écrire entre qotes « `'` » ou double quotes « `"` » .

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire5')}}  

On peut concaténer deux chaînes à l’aide de l’opérateur « `+` ».  
On peut connaitre la taille d’une chaîne à l’aide de la fonction `len( nom_de_la_chaîne )`.

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire6')}}

Que faire lorsqu’on a une apostrophe dans une variable str ?

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDEv('predires/predire7')}}


!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDEv('predires/predire8')}}

 
La séquence d'échappement `\n` représente un saut ligne. 


### 5.4. Le type bool (boolean : vrai ou faux)

Cette variable peut avoir deux valeurs :  
 - True (vrai);  
 - False (faux).  

Avec ces variables, on peut utiliser les opérateurs de comparaisons :  

| Opérateur | Signification         |
|-----------|-----------------------|
|     <     | strictement inférieur |
|     <=    | inférieur ou égal     |
|     >     | strictement supérieur |
|     >=    | supérieur ou égal     |
|     ==    | égal                  |
|     !=    | différent             |

On peut aussi utiliser les opérateurs logiques `and`, `or` et `not`.

!!! exo "Prédire/comprendre"
    Interpréter les résultats du programme suivant :
    {{IDE('predires/predire11')}}

  

On peut aussi utiliser l’opérateur `in` avec les chaînes de caractères.

!!! exo "Prédire/comprendre"
    Interpréter les résultats du programme suivant :
    {{IDEv('predires/predire12')}}


