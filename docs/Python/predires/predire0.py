a = 0   # la variable a est initialisée à la valeur 0.
print("valeur : ",a," - référence : ",id(a))

a = a+1 # la variable a est reliée à l'ancienne valeur de a augmentée de 1, c'est une incrémentation.
print("valeur : ",a," - référence : ",id(a))

a = 12 # la variable a change de valeur.
print("valeur : ",a," - référence : ",id(a))