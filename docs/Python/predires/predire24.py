compteur = 1
while compteur<=5:
    # ce bloc est exécuté tant que la condition (compteur<=5) est vraie
    print (compteur, "On est dans la boucle")
    compteur += 1    # incrémentation du compteur, compteur=compteur+1
print ("On sort de la boucle")
