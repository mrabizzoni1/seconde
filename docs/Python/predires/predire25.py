# Table de multiplication par 5
compteur = 1	# initialisation de la variable de comptage
while compteur <= 10:
# ce bloc est exécuté tant que la condition (compteur <= 10) est vraie
    print(compteur, '* 5 =', compteur*5)
    compteur += 1    
print("Et voilà !")
