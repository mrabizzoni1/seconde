t=("un","deux",3,4,(5,6))

# p a pour valeur le premier élément de t
p = t[0]

#d a pour valeur le dernier élément de t
d = t[len(t)-1]

#m a pour valeur le 3ème élément de t
m = t[2]


#Test
assert p==t[0]
assert d==t[len(t)-1]
assert m==t[2]