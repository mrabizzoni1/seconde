## POUR T
T = (1 , True , 2.0 , 'trois' , 5.0 , 'six' , 7 , False , 3.14 , (0,1))

#T1 est le tuple composée de tous les éléments d'indices impair
T1 = ...

#T2 est le tuple composée de tous les éléments des indices à partir de 3, jusqu’à la fin de la séquence.
T2 = ...

#T3 est l'élément d’indice 2 dans la séquence d’indice 5
T3 = ...

#T4 est l'élément d’indice 3 dans la dernière séquence du tuple
T4 = ...


## POUR L
L = [ ['a','b'] , "zero" , True , [1,2,3] , 10]

#L1 est la liste composée de tous les éléments d'indices pair
L1 = ...

#L2 est la liste composée des trois derniers éléments 
L2 = ...

#L3 est l'élément d’indice 2 dans la séquence d’indice 3
L3 = ...

#L4 est la liste composée de tous les éléments rangés dans l'ordre inverse
L4 = ...
