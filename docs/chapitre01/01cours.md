---
author: Maud RABIZZONI
title: Chapitre 1 - Calcul littéral
---

## 1. Vocabulaire

!!! example "EXEMPLE"

    Pour chaque expression, dire s'il s'agit d'une somme ou d'un produit.  
    $x – 3$  
    $(6x + 1)( x – 1)$  
    $2(1 + 6x)$  
    $(3 + 8x)(x – 8)²$  
    $(2x + 4) + 3x$  
    $3 + (2 + 3x)(x – 2)$  
    $(5 – x) – (9 + 9x)$  
    $(8 – x)(2 + x)$  


!!! info "REMARQUE"

    $\frac{3}{2-x}$ est appelé un quotient. C’est le produit de 3 et de l’inverse de 2 – x soit $3*\frac{1}{2-x}$.  

## Valeurs interdites 

Pour certaines expressions dépendantes de x, il existe des valeurs de x pour lesquelles on ne peut pas calculer l’expression.


!!! example "EXEMPLE"
    Soit $A(x) = \frac{x+5}{4+x}$.
    $A(x)$ est défini pour $x\neq-4$ 



## 2. Développer et factoriser

## 2.1. Distributivité

!!! abstract "DÉFINITION"	
    Développer c’est transformer un produit de facteurs en une somme (ou différence) de termes.  
    Factoriser c’est transformer une somme de termes en un produit de facteurs.  

!!! example "EXEMPLE"
    $x(4-y)=x×4+x×(-y)=4x-xy$


REMARQUE : 
On dit que la multiplication est distributive par rapport à l’addition (ou la soustraction). 
Dans l’exemple, on a distribué la multiplication par $x$  sur les termes $4$ et $-y$.

## 2.2. Double-distributivité

!!! danger "PROPRIÉTÉ"
    Soient $a$,$b$,$c$ et $d$ des nombres réels.  
    $(a+b)(c+d)=ac+ad+bc+bd$


## 2.3. Identités remarquables

!!! danger "PROPRIÉTÉ"
    Soient $a$ et $b$ des nombres réels positifs.  
    $(a+b)^2=a^2+2ab+b^2$  
    $(a-b)^2=a^2-2ab+b^2$  
    $(a+b)(a-b)=a^2-b^2$  

!!! example "EXEMPLE"
    $(x+5)^2=x^2+2×x×5+5^2=x^2+10x+25$  
    $(2x-3)^2=(2x)^2-2×2x×3+3^2=4x^2-12x+9$  
    $(1-2x)(1+2x)=1^2-(2x)^2=1-4x^2$  

???+ question "METHODES"
    Développer et réduire l'expression $A$.    
    $A=(x+2)(4x-3)-(7-x)^2$  
    Factoriser les expressions $B$, $C$, $D$ et $E$.  
    $B=3(2+3x)–(6+2x)(2+3x)$  
    $C=(2–5x)^2+(2–5x)(1+x)$  
    $D=(1–2x)–(4+3x)(2x–1)$  
    $E=(3x+1)^2–49$  
    ??? success "Solution"  
        $A=x×4x+x×(-3)+2×4x+2×(-3)$  
        $A=4x^2-3x+8x-6$  
        $A=4x^2+5x-6$  
        \
        $B=(2+3x)[3-(6+2x)]$  
        $B=(2+3x)(3-6-2x)$  
        $B=(2+3x)(-2x-3)$  
        $B=-(2+3x)(2x+3)$



## 4. Réduire au même dénominateur
	
!!! abstract "DÉFINITION"  
    Réduire au même dénominateur c'est transformer une somme (ou une différence) de deux quotients en un seul quotient.


!!! danger "PROPRIÉTÉ"  
    Pour tout nombre$$a$, $b$, $c$ et $d$, réels avec $b$ et $d$ non nuls on a :  
    $\frac{a}{b}+\frac{c}{d}=\frac{ad}{bd}+\frac{bc}{bd}=\frac{ad+bc}{bd}$

!!! example "EXEMPLE"  
    $A=\frac{7x}{x-2}-\frac{5}{3-x}$  
    $B=3+\frac{5x}{2x+1}$    
	
	
	


